package com.example.sensordataanalysis.Entidades;

import android.content.Context;
import android.widget.ArrayAdapter;

import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SensorInformationDTO
{
    String idUsuario;
    List<DataRecordDTO> dailyRecords;

    private static SensorInformationDTO instance;

    public static SensorInformationDTO getInstance()
    {
        if (instance == null)
        {
            instance = new SensorInformationDTO();
        }
        return instance;
    }

    private SensorInformationDTO()
    {
        idUsuario = "";
        dailyRecords = new ArrayList<>();
    }

    public void setIdUsuario (String usuario)
    {
        this.idUsuario = usuario;
    }

    public String getIdUsuario()
    {
        return this.idUsuario;
    }

    public List<DataRecordDTO> getDailyRecord()
    {
        return this.dailyRecords;
    }

    public boolean addValueToRecords(DataRecordDTO dr)
    {
        return dailyRecords.add(dr);
    }

    public void limpiarLista()
    {
        dailyRecords.clear();
    }

    public void setLista(ArrayList<DataRecordDTO> lista){this.dailyRecords = lista;}

}
