package com.example.sensordataanalysis.Entidades;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.greenrobot.greendao.database.Database;

public class MasterDao {
    public static String TAG = "DAOMaster";
    private static MasterDao instance;
    private DaoMaster.DevOpenHelper helper;
    private Database db;
    private DaoMaster daoMaster;
    private DaoSession daoSession;
    public static MasterDao getInstance(Context context)
    {
        if (instance == null)
        {
            instance = new MasterDao(context);
        }
        return instance;
    }

    private MasterDao(Context context)
    {
        helper = new DaoMaster.DevOpenHelper(context, "notes-db", null);
        db = helper.getWritableDb();
        daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();
        Log.d(TAG, "Inicializada la base de datos");
        //DaoMaster.dropAllTables(db, true);
        //DaoMaster.createAllTables(db, true);


    }

    public DaoSession getDaoSession()
    {
        return this.daoSession;
    }
}
