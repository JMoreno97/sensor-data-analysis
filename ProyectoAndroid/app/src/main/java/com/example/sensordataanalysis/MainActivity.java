package com.example.sensordataanalysis;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.PermissionChecker;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.aware.Aware;
import com.example.sensordataanalysis.Entidades.MasterDao;
import com.example.sensordataanalysis.Entidades.DaoSession;
import com.example.sensordataanalysis.Entidades.Sensor;
import com.example.sensordataanalysis.Entidades.SensorInformation;
import com.example.sensordataanalysis.Entidades.SensorInformationDao;
import com.example.sensordataanalysis.Utils.SwitchRecyclerAdapter;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MainActivity";
    private RecyclerView recyclerView;
    private LinearLayoutManager layoutMgr;
    private SwitchRecyclerAdapter mAdapter;
    public static ArrayList<Sensor> sensores;
    public static final String appPreferences = "SensorPreferences";
    private SharedPreferences preferences;
    private static  ArrayList<String> REQUIRED_PERMISSIONS = new ArrayList<>();
    private static int PERMISSION_REQUEST_FINE = 0;
    private TextInputEditText nombre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "Aplicación lanzada");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MasterDao.getInstance(this);
        sensores = new ArrayList<>();
        nombre = (TextInputEditText) findViewById(R.id.tiet_nombre);
        preferences = getSharedPreferences(appPreferences,0);
        String usuario = preferences.getString("Usuario", "");
        if(!usuario.equals(""))
        {
            nombre.setText(usuario);
        }
        initPermissions();
        initSensorList();
        initSensorLayout();
        Log.d(TAG,"Aplicación inicializada");

    }

    private void initSensorList()
    {
        sensores.add(new Sensor("Bateria",preferences.getInt("Bateria Integer",60000),preferences.getBoolean("Bateria Boolean",false),0));
        sensores.add(new Sensor("Luminosidad",preferences.getInt("Luminosidad Integer",60000),preferences.getBoolean("Luminosidad Boolean",false),1));
        sensores.add(new Sensor("Localizacion",preferences.getInt("Localizacion Integer",60000),preferences.getBoolean("Localizacion Boolean",false),2));
        sensores.add(new Sensor("Pantalla",preferences.getInt("Pantalla Integer",60000),preferences.getBoolean("Pantalla Boolean",false),3));
        sensores.add(new Sensor("Wifi",preferences.getInt("Wifi Integer",60000),preferences.getBoolean("Wifi Boolean",false),4));
        sensores.add(new Sensor("Bluetooth",preferences.getInt("Bluetooth Integer",60000),preferences.getBoolean("Bluetooth Boolean",false),5));
        sensores.add(new Sensor("Zona horaria",preferences.getInt("Zona horaria Integer",60000),preferences.getBoolean("Zona horaria Boolean",false),6));
    }

    private void initSensorLayout()
    {
        recyclerView = (RecyclerView) findViewById(R.id.list_sensores);
        layoutMgr = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutMgr);

        mAdapter = new SwitchRecyclerAdapter(this, sensores);
        recyclerView.setAdapter(mAdapter);
        mAdapter.setOnCheckedChangeListener(mOnCheckedChangeListener);
        mAdapter.setOnClickListener(mOnClickedListener);
    }

    private SwitchRecyclerAdapter.OnCheckedChangeListener mOnCheckedChangeListener = new SwitchRecyclerAdapter.OnCheckedChangeListener() {

        @Override
        public void onCheckedChanged(int position, boolean isChecked) {
            Log.d(TAG, ">>> position : " + position + ", isChecked : " + isChecked);
            sensores.get(position).setActivo(isChecked);
        }
    };

    private SwitchRecyclerAdapter.OnItemClickedListener mOnClickedListener = new SwitchRecyclerAdapter.OnItemClickedListener() {
        @Override
        public void onItemClicked(Sensor s, int position) {
            Log.d(TAG, ">>>  sensor : " + s.getNombre() + ", position : " + position);
        }
    };


    private void iniciarServicio()
    {
        Log.d(TAG,"Intentando iniciar servicio");
        DaoSession session = MasterDao.getInstance(this).getDaoSession();
        SensorInformationDao sensorInformationDao = session.getSensorInformationDao();
        SensorInformation sensorInformation = sensorInformationDao.queryBuilder().where(SensorInformationDao.Properties.IdUsuario.eq(nombre.getText().toString())).unique();
        if(sensorInformation == null)
        {
            Log.d(TAG,"El usuario "+nombre.getText().toString()+" no estaba registrado en el sistema");
            SensorInformation sensorInformation2 = new SensorInformation(nombre.getText().toString());
            sensorInformationDao.insert(sensorInformation2);
        }else
        {
            Log.d(TAG,"El usuario "+nombre.getText().toString()+" ya estaba registrado en el sistema");
        }
        Intent collectDataIntent = new Intent(this, DataStoreService.class);
        ContextCompat.startForegroundService(this, collectDataIntent);
        Log.d(TAG,"Servicio iniciado");
        this.finish();
    }

    private void reiniciarServicio()
    {
        Intent collectDataIntent = new Intent(this, DataStoreService.class);
        this.stopService(collectDataIntent);
        Log.d(TAG,"Servicio detenido");
        iniciarServicio();
    }

    private void pararServicio()
    {
        Intent collectDataIntent = new Intent(this, DataStoreService.class);
        this.stopService(collectDataIntent);
        Aware.stopAWARE(this);
    }

    public void onClick(View view)
    {
        SharedPreferences.Editor editor = preferences.edit();
        Log.d(TAG,"El usuario que se almacena es: "+nombre.getText().toString());
        editor.putString("Usuario",nombre.getText().toString());
        editor.commit();
        boolean activo = false;
        for(Sensor sensor: sensores)
        {
            editor = preferences.edit();
            editor.putBoolean(sensor.getNombre()+" Boolean", sensor.getActivo());
            editor.putInt(sensor.getNombre()+" Integer",sensor.getTiempo());
            editor.commit();

            if(sensor.getActivo() && !activo)
            {
                activo = true;
            }

        }
        if(activo)
            reiniciarServicio();
        else
            pararServicio();
    }

    public void initPermissions()
    {

        REQUIRED_PERMISSIONS.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        REQUIRED_PERMISSIONS.add(Manifest.permission.ACCESS_WIFI_STATE);
        REQUIRED_PERMISSIONS.add(Manifest.permission.CAMERA);
        REQUIRED_PERMISSIONS.add(Manifest.permission.BLUETOOTH);
        REQUIRED_PERMISSIONS.add(Manifest.permission.BLUETOOTH_ADMIN);
        REQUIRED_PERMISSIONS.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        REQUIRED_PERMISSIONS.add(Manifest.permission.ACCESS_FINE_LOCATION);
        REQUIRED_PERMISSIONS.add(Manifest.permission.READ_PHONE_STATE);
        REQUIRED_PERMISSIONS.add(Manifest.permission.GET_ACCOUNTS);
        REQUIRED_PERMISSIONS.add(Manifest.permission.WRITE_SYNC_SETTINGS);
        REQUIRED_PERMISSIONS.add(Manifest.permission.READ_SYNC_SETTINGS);
        REQUIRED_PERMISSIONS.add(Manifest.permission.READ_SYNC_STATS);
        REQUIRED_PERMISSIONS.add(Manifest.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
        REQUIRED_PERMISSIONS.add(Manifest.permission.FOREGROUND_SERVICE);

        for (String p : REQUIRED_PERMISSIONS) {
            if (PermissionChecker.checkSelfPermission(this, p) != PermissionChecker.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{p},PERMISSION_REQUEST_FINE);
            }
        }

    }


}
