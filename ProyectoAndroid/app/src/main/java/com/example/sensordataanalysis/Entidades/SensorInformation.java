package com.example.sensordataanalysis.Entidades;

import android.util.Log;
import android.widget.ArrayAdapter;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.ToMany;

import java.util.ArrayList;
import java.util.List;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.DaoException;

@Entity
public class SensorInformation {

    @Id
    private String idUsuario;
    @ToMany(referencedJoinProperty = "sensorInformationId")
    List<DataRecords> dailyRecords;
    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;
    /** Used for active entity operations. */
    @Generated(hash = 1235806032)
    private transient SensorInformationDao myDao;
    @Generated(hash = 207973140)
    public SensorInformation(String idUsuario) {
        this.idUsuario = idUsuario;
    }
    @Generated(hash = 445767785)
    public SensorInformation() {
    }

    public String getIdUsuario() {
        return this.idUsuario;
    }
    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }
    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 763716866)
    public List<DataRecords> getDailyRecords() {
        if (dailyRecords == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            DataRecordsDao targetDao = daoSession.getDataRecordsDao();
            List<DataRecords> dailyRecordsNew = targetDao
                    ._querySensorInformation_DailyRecords(idUsuario);
            synchronized (this) {
                if (dailyRecords == null) {
                    dailyRecords = dailyRecordsNew;
                }
            }
        }
        return dailyRecords;
    }
    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    @Generated(hash = 1287280145)
    public synchronized void resetDailyRecords() {
        dailyRecords = null;
    }
    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }
    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }
    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }
    public void toDTO()
    {
        ArrayList<DataRecordDTO> dataRecordDTOList = new ArrayList<>();
        Log.d("SensorInformation", "Hay "+getDailyRecords().size()+" registros");
        for(DataRecords dataRecords : getDailyRecords())
        {
            DataRecordDTO dataRecordDTO = dataRecords.toDTO();
            dataRecordDTOList.add(dataRecordDTO);
        }

        SensorInformationDTO.getInstance().setIdUsuario(idUsuario);
        SensorInformationDTO.getInstance().setLista(dataRecordDTOList);
    }
    public void addDataRecords(DataRecords dataRecords)
    {
        dailyRecords.add(dataRecords);
    }

    public void cleanDataRecords()
    {
        getDailyRecords().clear();
    }
    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 1307308165)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getSensorInformationDao() : null;
    }
}
