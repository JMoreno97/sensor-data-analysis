package com.example.sensordataanalysis;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.aware.Aware;
import com.aware.Aware_Preferences;
import com.aware.Battery;
import com.aware.Bluetooth;
import com.aware.Light;
import com.aware.Locations;
import com.aware.Screen;
import com.aware.Timezone;
import com.aware.WiFi;
import com.aware.providers.Battery_Provider;
import com.aware.providers.Light_Provider;
import com.aware.providers.Locations_Provider;
import com.aware.providers.TimeZone_Provider;
import com.example.sensordataanalysis.Entidades.MasterDao;
import com.example.sensordataanalysis.Entidades.DaoSession;
import com.example.sensordataanalysis.Entidades.DataRecords;
import com.example.sensordataanalysis.Entidades.DataRecordsDao;
import com.example.sensordataanalysis.Entidades.Sensor;
import com.example.sensordataanalysis.Entidades.SensorInformation;
import com.example.sensordataanalysis.Entidades.SensorInformationDTO;
import com.example.sensordataanalysis.Entidades.SensorInformationDao;

import java.util.Calendar;

import static com.example.sensordataanalysis.MainActivity.appPreferences;
import static com.example.sensordataanalysis.MainActivity.sensores;

public class DataStoreService extends Service {

    private static final int NOTIF_ID = 1;
    private static final String NOTIF_CHANNEL_ID = "Recolección de Datos";
    private static final String NOTIF_CHANNEL_DESCP = "Notificación del servicio de recolección de datos";
    private final String TAG = "DataStoreService";
    private SharedPreferences preferences;

    public DataStoreService() {
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        Log.d(TAG, "Servicio iniciado");
        preferences = getSharedPreferences(appPreferences,0);
        SensorInformationDTO.getInstance().setIdUsuario(preferences.getString("Usuario","Default"));
        createNotificationChannel();
        startForeground();
        Log.d(TAG, "Inicializando servicio AWARE");
        Aware.startAWARE(getApplicationContext());
        Log.d(TAG, "Servicio AWARE inicializado");
        initializeSensors();
        startAlarm();
        return Service.START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    private void startForeground() {
        Log.d(TAG,"Servicio en segundo plano");
        Intent notificationIntent = new Intent(this, MainActivity.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);

        startForeground(NOTIF_ID, new NotificationCompat.Builder(this,
                NOTIF_CHANNEL_ID) // don't forget create a notification channel first
                .setOngoing(true)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(getString(R.string.app_name))
                .setContentText("El servicio se está ejecutando en segundo plano")
                .setContentIntent(pendingIntent)
                .build());
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = NOTIF_CHANNEL_ID;
            String description = NOTIF_CHANNEL_DESCP;
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(NOTIF_CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    public void startAlarm()
    {
        Log.d(TAG,"Intentando crear alarma");
        AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this,DataSendService.class);
        Log.d(TAG, "El usuario que se pasa es "+preferences.getString("Usuario","null"));
        intent.putExtra("Usuario", preferences.getString("Usuario","null"));
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 1, intent,PendingIntent.FLAG_UPDATE_CURRENT);
        if(PendingIntent.getBroadcast(this, 0, intent,PendingIntent.FLAG_NO_CREATE) == null)
        {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(System.currentTimeMillis());
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            alarmManager.setInexactRepeating(alarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
            Log.d(TAG,"Alarma creada");
        }

    }

    public void cancelAlarm()
    {
        Log.d(TAG,"Alarma cancelada");
        AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this,DataSendService.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 1, intent,0);

        alarmManager.cancel(pendingIntent);

    }


    private void initializeSensors()
    {
        Log.d(TAG,"Inicializando sensores...");
        for (Sensor sensor: sensores)
        {
            Log.d(TAG, "Sensor: "+sensor.getNombre()+" Tiempo: "+sensor.getTiempo());
            if(preferences.getBoolean(sensor.getNombre()+" Boolean", false))
            {
                switch (sensor.getTipo())
                {
                    case 0:
                    {
                        Aware.setSetting(this, Aware_Preferences.STATUS_BATTERY, sensor.getActivo());
                        Aware.startBattery(this);
                        Battery.setSensorObserver(new Battery.AWARESensorObserver() {
                            @Override
                            public void onBatteryChanged(ContentValues data) {
                                Log.d(TAG,"Recibidos datos de bateria");
                                long moment = data.getAsDouble(Battery_Provider.Battery_Data.TIMESTAMP).longValue();
                                int value = data.getAsInteger(Battery_Provider.Battery_Data.LEVEL);
                                DataRecords dataRecords = new DataRecords("Bateria", "BatteryChanged", moment, Integer.toString(value),"Integer");

                                //Inserción en BD
                                DaoSession session =  MasterDao.getInstance(getApplicationContext()).getDaoSession();
                                DataRecordsDao dataRecordsDao = session.getDataRecordsDao();
                                dataRecords.setSensorInformationId(preferences.getString("Usuario","null"));
                                dataRecordsDao.insert(dataRecords);
                                SensorInformationDao sensorInformationDao = session.getSensorInformationDao();
                                SensorInformation sensorInformation = sensorInformationDao.queryBuilder()
                                        .where(SensorInformationDao.Properties.IdUsuario.eq(preferences.getString("Usuario","null")))
                                        .unique();
                                sensorInformation.getDailyRecords().add(dataRecords);
                            }

                            @Override
                            public void onPhoneReboot() {

                            }

                            @Override
                            public void onPhoneShutdown() {

                            }

                            @Override
                            public void onBatteryLow() {
                                Log.d(TAG,"Recibidos datos de bateria");
                                long moment =  System.currentTimeMillis();
                                boolean value = true;
                                DataRecords dataRecords = new DataRecords("Battery", "BatteryLow", moment, Boolean.toString(value),"Boolean");

                                //Inserción en BD
                                DaoSession session =  MasterDao.getInstance(getApplicationContext()).getDaoSession();
                                DataRecordsDao dataRecordsDao = session.getDataRecordsDao();
                                dataRecords.setSensorInformationId(preferences.getString("Usuario","null"));
                                dataRecordsDao.insert(dataRecords);
                                SensorInformationDao sensorInformationDao = session.getSensorInformationDao();
                                SensorInformation sensorInformation = sensorInformationDao.queryBuilder()
                                        .where(SensorInformationDao.Properties.IdUsuario.eq(preferences.getString("Usuario","null")))
                                        .unique();
                                sensorInformation.getDailyRecords().add(dataRecords);

                            }

                            @Override
                            public void onBatteryCharging() {
                                Log.d(TAG,"Recibidos datos de bateria");
                                long moment =  System.currentTimeMillis();
                                boolean value = true;
                                DataRecords dataRecords = new DataRecords("Battery", "BatteryCharging", moment, Boolean.toString(value),"Boolean");

                                //Inserción en BD
                                DaoSession session =  MasterDao.getInstance(getApplicationContext()).getDaoSession();
                                DataRecordsDao dataRecordsDao = session.getDataRecordsDao();
                                dataRecords.setSensorInformationId(preferences.getString("Usuario","null"));
                                dataRecordsDao.insert(dataRecords);
                                SensorInformationDao sensorInformationDao = session.getSensorInformationDao();
                                SensorInformation sensorInformation = sensorInformationDao.queryBuilder()
                                        .where(SensorInformationDao.Properties.IdUsuario.eq(preferences.getString("Usuario","null")))
                                        .unique();
                                sensorInformation.getDailyRecords().add(dataRecords);
                            }

                            @Override
                            public void onBatteryDischarging() {
                                Log.d(TAG,"Recibidos datos de bateria");
                                long moment =  System.currentTimeMillis();
                                boolean value = true;
                                DataRecords dataRecords = new DataRecords("Battery", "BatteryDischarging", moment, Boolean.toString(value),"Boolean");

                                //Inserción en BD
                                DaoSession session =  MasterDao.getInstance(getApplicationContext()).getDaoSession();
                                DataRecordsDao dataRecordsDao = session.getDataRecordsDao();
                                dataRecords.setSensorInformationId(preferences.getString("Usuario","null"));
                                dataRecordsDao.insert(dataRecords);
                                SensorInformationDao sensorInformationDao = session.getSensorInformationDao();
                                SensorInformation sensorInformation = sensorInformationDao.queryBuilder()
                                        .where(SensorInformationDao.Properties.IdUsuario.eq(preferences.getString("Usuario","null")))
                                        .unique();
                                sensorInformation.getDailyRecords().add(dataRecords);
                            }
                        });

                        break;
                    }
                    case 1:
                    {
                        Aware.setSetting(this, Aware_Preferences.STATUS_LIGHT, sensor.getActivo());
                        Aware.setSetting(this, Aware_Preferences.FREQUENCY_LIGHT, sensor.getTiempo());
                        Aware.setSetting(this, Aware_Preferences.THRESHOLD_LIGHT, 50);
                        Aware.startLight(this);
                        Light.setSensorObserver(new Light.AWARESensorObserver() {
                            @Override
                            public void onLightChanged(ContentValues data) {
                                Log.d(TAG,"Recibidos datos de luminosidad");
                                long moment = data.getAsDouble(Light_Provider.Light_Data.TIMESTAMP).longValue();
                                double value = data.getAsDouble(Light_Provider.Light_Data.LIGHT_LUX);
                                DataRecords dataRecords = new DataRecords("Light", "LightChanged", moment, Double.toString(value),"Double");

                                //Inserción en BD
                                DaoSession session =  MasterDao.getInstance(getApplicationContext()).getDaoSession();
                                DataRecordsDao dataRecordsDao = session.getDataRecordsDao();
                                dataRecords.setSensorInformationId(preferences.getString("Usuario","null"));
                                dataRecordsDao.insert(dataRecords);
                                SensorInformationDao sensorInformationDao = session.getSensorInformationDao();
                                SensorInformation sensorInformation = sensorInformationDao.queryBuilder()
                                        .where(SensorInformationDao.Properties.IdUsuario.eq(preferences.getString("Usuario","null")))
                                        .unique();
                                sensorInformation.getDailyRecords().add(dataRecords);
                            }
                        });
                        break;
                    }
                    case 2:
                    {
                        Aware.setSetting(this, Aware_Preferences.STATUS_LOCATION_GPS, sensor.getActivo());
                        Aware.setSetting(this, Aware_Preferences.FREQUENCY_LOCATION_GPS, sensor.getTiempo());
                        Aware.startLocations(this);
                        Locations.setSensorObserver(new Locations.AWARESensorObserver() {
                            @Override
                            public void onLocationChanged(ContentValues data) {
                                Log.d(TAG,"Recibidos datos de Localizacion");
                                long moment = data.getAsDouble(Locations_Provider.Locations_Data.TIMESTAMP).longValue();
                                double latitud = data.getAsDouble(Locations_Provider.Locations_Data.LATITUDE);
                                double longitud = data.getAsDouble(Locations_Provider.Locations_Data.LONGITUDE);
                                String value = Double.toString(latitud)+","+Double.toString(longitud);
                                DataRecords dataRecords = new DataRecords("Location", "LocationChanged", moment, value,"Double");

                                //Inserción en BD
                                DaoSession session =  MasterDao.getInstance(getApplicationContext()).getDaoSession();
                                DataRecordsDao dataRecordsDao = session.getDataRecordsDao();
                                dataRecords.setSensorInformationId(preferences.getString("Usuario","null"));
                                dataRecordsDao.insert(dataRecords);
                                SensorInformationDao sensorInformationDao = session.getSensorInformationDao();
                                SensorInformation sensorInformation = sensorInformationDao.queryBuilder()
                                        .where(SensorInformationDao.Properties.IdUsuario.eq(preferences.getString("Usuario","null")))
                                        .unique();
                                sensorInformation.getDailyRecords().add(dataRecords);
                            }
                        });
                        break;
                    }
                    case 3:
                    {
                        Aware.setSetting(this, Aware_Preferences.STATUS_SCREEN, sensor.getActivo());
                        Aware.startScreen(this);
                        Screen.setSensorObserver(new Screen.AWARESensorObserver() {
                            @Override
                            public void onScreenOn() {
                                Log.d(TAG,"Recibidos datos de pantalla");
                                long moment = System.currentTimeMillis();
                                boolean value = true;
                                DataRecords dataRecords = new DataRecords("Screen", "ScreenOn", moment, Boolean.toString(value),"Boolean");

                                //Inserción en BD
                                DaoSession session =  MasterDao.getInstance(getApplicationContext()).getDaoSession();
                                DataRecordsDao dataRecordsDao = session.getDataRecordsDao();
                                dataRecords.setSensorInformationId(preferences.getString("Usuario","null"));
                                dataRecordsDao.insert(dataRecords);
                                SensorInformationDao sensorInformationDao = session.getSensorInformationDao();
                                SensorInformation sensorInformation = sensorInformationDao.queryBuilder()
                                        .where(SensorInformationDao.Properties.IdUsuario.eq(preferences.getString("Usuario","null")))
                                        .unique();
                                sensorInformation.getDailyRecords().add(dataRecords);
                            }

                            @Override
                            public void onScreenOff() {
                                Log.d(TAG,"Recibidos datos de pantalla");
                                long moment =  System.currentTimeMillis();
                                boolean value = true;
                                DataRecords dataRecords = new DataRecords("Screen", "ScreenOff", moment, Boolean.toString(value),"Boolean");

                                //Inserción en BD
                                DaoSession session =  MasterDao.getInstance(getApplicationContext()).getDaoSession();
                                DataRecordsDao dataRecordsDao = session.getDataRecordsDao();
                                dataRecords.setSensorInformationId(preferences.getString("Usuario","null"));
                                dataRecordsDao.insert(dataRecords);
                                SensorInformationDao sensorInformationDao = session.getSensorInformationDao();
                                SensorInformation sensorInformation = sensorInformationDao.queryBuilder()
                                        .where(SensorInformationDao.Properties.IdUsuario.eq(preferences.getString("Usuario","null")))
                                        .unique();
                                sensorInformation.getDailyRecords().add(dataRecords);
                            }

                            @Override
                            public void onScreenLocked() {
                                Log.d(TAG,"Recibidos datos de pantalla");
                                long moment =  System.currentTimeMillis();
                                boolean value = true;
                                DataRecords dataRecords = new DataRecords("Screen", "ScreenLocked", moment, Boolean.toString(value),"Boolean");

                                //Inserción en BD
                                DaoSession session =  MasterDao.getInstance(getApplicationContext()).getDaoSession();
                                DataRecordsDao dataRecordsDao = session.getDataRecordsDao();
                                dataRecords.setSensorInformationId(preferences.getString("Usuario","null"));
                                dataRecordsDao.insert(dataRecords);
                                SensorInformationDao sensorInformationDao = session.getSensorInformationDao();
                                SensorInformation sensorInformation = sensorInformationDao.queryBuilder()
                                        .where(SensorInformationDao.Properties.IdUsuario.eq(preferences.getString("Usuario","null")))
                                        .unique();
                                sensorInformation.getDailyRecords().add(dataRecords);
                            }

                            @Override
                            public void onScreenUnlocked() {
                                Log.d(TAG,"Recibidos datos de pantalla");
                                long moment =  System.currentTimeMillis();
                                boolean value = true;
                                DataRecords dataRecords = new DataRecords("Screen", "ScreenUnlocked", moment, Boolean.toString(value),"Boolean");

                                //Inserción en BD
                                DaoSession session =  MasterDao.getInstance(getApplicationContext()).getDaoSession();
                                DataRecordsDao dataRecordsDao = session.getDataRecordsDao();
                                dataRecords.setSensorInformationId(preferences.getString("Usuario","null"));
                                dataRecordsDao.insert(dataRecords);
                                SensorInformationDao sensorInformationDao = session.getSensorInformationDao();
                                SensorInformation sensorInformation = sensorInformationDao.queryBuilder()
                                        .where(SensorInformationDao.Properties.IdUsuario.eq(preferences.getString("Usuario","null")))
                                        .unique();
                                sensorInformation.getDailyRecords().add(dataRecords);
                            }
                        });
                        break;
                    }
                    case 4:
                    {
                        Aware.setSetting(this, Aware_Preferences.STATUS_WIFI, sensor.getActivo());
                        Aware.setSetting(this, Aware_Preferences.FREQUENCY_WIFI, sensor.getTiempo());
                        Aware.startWiFi(this);
                        WiFi.setSensorObserver(new WiFi.AWARESensorObserver() {
                            @Override
                            public void onWiFiAPDetected(ContentValues data) {

                            }

                            @Override
                            public void onWiFiDisabled() {
                                Log.d(TAG,"Recibidos datos de wifi");
                                long moment =  System.currentTimeMillis();
                                boolean value = true;
                                DataRecords dataRecords = new DataRecords("Wifi", "WifiDisabled", moment, Boolean.toString(value),"Boolean");

                                //Inserción en BD
                                DaoSession session =  MasterDao.getInstance(getApplicationContext()).getDaoSession();
                                DataRecordsDao dataRecordsDao = session.getDataRecordsDao();
                                dataRecords.setSensorInformationId(preferences.getString("Usuario","null"));
                                dataRecordsDao.insert(dataRecords);
                                SensorInformationDao sensorInformationDao = session.getSensorInformationDao();
                                SensorInformation sensorInformation = sensorInformationDao.queryBuilder()
                                        .where(SensorInformationDao.Properties.IdUsuario.eq(preferences.getString("Usuario","null")))
                                        .unique();
                                sensorInformation.getDailyRecords().add(dataRecords);
                            }

                            @Override
                            public void onWiFiScanStarted() {

                            }

                            @Override
                            public void onWiFiScanEnded() {

                            }
                        });
                        break;
                    }
                    case 5:
                    {
                        Aware.setSetting(this, Aware_Preferences.STATUS_BLUETOOTH, sensor.getActivo());
                        Aware.setSetting(this, Aware_Preferences.FREQUENCY_BLUETOOTH, sensor.getTiempo());
                        Aware.startBluetooth(this);
                        Bluetooth.setSensorObserver(new Bluetooth.AWARESensorObserver() {
                            @Override
                            public void onBluetoothDetected(ContentValues data) {

                            }

                            @Override
                            public void onBluetoothBLEDetected(ContentValues data) {

                            }

                            @Override
                            public void onScanStarted() {

                            }

                            @Override
                            public void onScanEnded() {

                            }

                            @Override
                            public void onBLEScanStarted() {

                            }

                            @Override
                            public void onBLEScanEnded() {

                            }

                            @Override
                            public void onBluetoothDisabled() {
                                Log.d(TAG,"Recibidos datos de bluetooth");
                                long moment =  System.currentTimeMillis();
                                boolean value = true;
                                DataRecords dataRecords = new DataRecords("Bluetooth", "BluetoothDisabled", moment, Boolean.toString(value),"Boolean");

                                //Inserción en BD
                                DaoSession session =  MasterDao.getInstance(getApplicationContext()).getDaoSession();
                                DataRecordsDao dataRecordsDao = session.getDataRecordsDao();
                                dataRecords.setSensorInformationId(preferences.getString("Usuario","null"));
                                dataRecordsDao.insert(dataRecords);
                                SensorInformationDao sensorInformationDao = session.getSensorInformationDao();
                                SensorInformation sensorInformation = sensorInformationDao.queryBuilder()
                                        .where(SensorInformationDao.Properties.IdUsuario.eq(preferences.getString("Usuario","null")))
                                        .unique();
                                sensorInformation.getDailyRecords().add(dataRecords);
                            }
                        });
                        break;
                    }
                    case 6:
                    {
                        Aware.setSetting(this, Aware_Preferences.STATUS_TIMEZONE, sensor.getActivo());
                        Aware.setSetting(this, Aware_Preferences.FREQUENCY_TIMEZONE, sensor.getTiempo());
                        Aware.startTimeZone(this);
                        Timezone.setSensorObserver(new Timezone.AWARESensorObserver() {
                            @Override
                            public void onTimezoneChanged(ContentValues data) {
                                Log.d(TAG,"Recibidos datos de Timezone");
                                long moment = data.getAsDouble(TimeZone_Provider.TimeZone_Data.TIMESTAMP).longValue();
                                String value = data.getAsString(TimeZone_Provider.TimeZone_Data.TIMEZONE);
                                DataRecords dataRecords = new DataRecords("Timezone", "TimezoneChanged", moment, value,"String");

                                //Inserción en BD
                                DaoSession session =  MasterDao.getInstance(getApplicationContext()).getDaoSession();
                                DataRecordsDao dataRecordsDao = session.getDataRecordsDao();
                                dataRecords.setSensorInformationId(preferences.getString("Usuario","null"));
                                dataRecordsDao.insert(dataRecords);
                                SensorInformationDao sensorInformationDao = session.getSensorInformationDao();
                                SensorInformation sensorInformation = sensorInformationDao.queryBuilder()
                                        .where(SensorInformationDao.Properties.IdUsuario.eq(preferences.getString("Usuario","null")))
                                        .unique();
                                sensorInformation.getDailyRecords().add(dataRecords);
                            }
                        });
                        break;
                    }
                }
            }
        }
        Log.d(TAG,"Sensores inicializados");
    }
}