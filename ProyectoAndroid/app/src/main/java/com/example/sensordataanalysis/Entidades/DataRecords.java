package com.example.sensordataanalysis.Entidades;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class DataRecords {
    @Id(autoincrement = true)
    private Long id;

    private String sensor;

    private String medida;

    private long moment;

    private String value;

    private String tipo;

    private String sensorInformationId;

    @Generated(hash = 1562052290)
    public DataRecords(Long id, String sensor, String medida, long moment, String value,
            String tipo, String sensorInformationId) {
        this.id = id;
        this.sensor = sensor;
        this.medida = medida;
        this.moment = moment;
        this.value = value;
        this.tipo = tipo;
        this.sensorInformationId = sensorInformationId;
    }

    @Generated(hash = 1179119023)
    public DataRecords() {
    }

    public DataRecords(String sensor, String medida, long moment, String value, String tipo) {
        this.sensor = sensor;
        this.medida = medida;
        this.moment = moment;
        this.value = value;
        this.tipo = tipo;
    }


    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSensor() {
        return this.sensor;
    }

    public void setSensor(String sensor) {
        this.sensor = sensor;
    }

    public String getMedida() {
        return this.medida;
    }

    public void setMedida(String medida) {
        this.medida = medida;
    }

    public long getMoment() {
        return this.moment;
    }

    public void setMoment(long moment) {
        this.moment = moment;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getSensorInformationId() {
        return this.sensorInformationId;
    }

    public void setSensorInformationId(String sensorInformationId) {
        this.sensorInformationId = sensorInformationId;
    }

    public DataRecordDTO toDTO()
    {
        DataRecordDTO dataRecordDTO = new DataRecordDTO(sensor, medida, moment, value, tipo);
        return dataRecordDTO;
    }
}
