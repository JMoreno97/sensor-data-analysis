package com.example.sensordataanalysis.Entidades;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.time.LocalDateTime;
import java.util.Date;

public class DataRecordDTO {

    String sensor;

    String medida;

    long moment;

    String value;

    String tipo;


    public DataRecordDTO(String sensor, String medida, long moment, String value, String tipo) {
        this.sensor = sensor;
        this.moment = moment;
        this.value = value;
        this.tipo = tipo;
        this.medida = medida;

    }

    public void printData()
    {
        System.out.println("Sensor: "+ sensor+" Medida: "+medida+" Timestamp: "+Long.toString(moment)+" Value: "+value+" Tipo: "+tipo);
    }

}
