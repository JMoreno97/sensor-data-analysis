package com.example.sensordataanalysis;

import com.example.sensordataanalysis.Entidades.SensorInformationDTO;

import javax.xml.transform.Result;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface DataRecordsInterface {
    @Headers("Content-Type: application/json")
    @POST("sensor/data")
    Call<ResponseBody> sendInformation(@Body SensorInformationDTO sensorInformationDTO);
}
