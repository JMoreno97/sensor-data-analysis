package com.example.sensordataanalysis.Utils;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.sensordataanalysis.Entidades.Sensor;
import com.example.sensordataanalysis.R;
import com.google.android.material.switchmaterial.SwitchMaterial;
import com.google.android.material.textfield.TextInputEditText;

import java.util.List;

public class SwitchRecyclerAdapter extends RecyclerView.Adapter<SwitchRecyclerAdapter.ViewHolder>{

    private Context mContext = null;
    private List<Sensor> mDataList = null;

    private OnItemClickedListener mItemClickListener = null;
    private OnCheckedChangeListener onItemCheckedListener = null;

    public interface OnItemClickedListener {
        void onItemClicked(Sensor vo, int position);
    }

    public interface OnCheckedChangeListener {
        void onCheckedChanged(int position , boolean isChecked);
    }


    public SwitchRecyclerAdapter(Context context, List<Sensor> items) {
        mContext = context;
        mDataList = items;
    }

    public void setOnClickListener(OnItemClickedListener listener) {
        mItemClickListener = listener;
    }

    public void setOnCheckedChangeListener(OnCheckedChangeListener listener) {
        onItemCheckedListener = listener;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_lista_sensores, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Sensor item = mDataList.get(position);
        holder.nombreSensor.setText(item.getNombre());
        holder.tiempo.setText(Integer.toString(item.getTiempo()));
        holder.tiempo.setTag(position);
        holder.tiempo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.equals("")) {
                    try {
                        Integer.parseInt(s.toString());
                        mDataList.get((int) holder.tiempo.getTag()).setTiempo(Integer.valueOf(s.toString()));
                    } catch (NumberFormatException ex) {
                        Log.d("Adapter", "No se puede convertir a numero");
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        holder.switchBtn.setChecked(item.getActivo());

        holder.switchBtn.setTag(position);
        holder.switchBtn.setOnCheckedChangeListener(mOnCheckedChangeListener);

        holder.itemView.setTag(position);
        holder.itemView.setOnClickListener(mOnClickListener);

    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView nombreSensor;
        TextInputEditText tiempo;
        SwitchMaterial switchBtn;

        public ViewHolder(View itemView) {
            super(itemView);
            nombreSensor = (TextView) itemView.findViewById(R.id.tv_nombre_sensor);
            tiempo = (TextInputEditText) itemView.findViewById(R.id.tiet_tiempo_medir) ;
            switchBtn = (SwitchMaterial) itemView.findViewById(R.id.sw_medir_sensor);
        }
    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int position = (int) v.getTag();

            if (mItemClickListener != null) {
                mItemClickListener.onItemClicked(mDataList.get(position), position);
            }
        }
    };

    private SwitchMaterial.OnCheckedChangeListener mOnCheckedChangeListener = new SwitchMaterial.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            int position = (int) buttonView.getTag();

            if (onItemCheckedListener != null) {
                onItemCheckedListener.onCheckedChanged(position, isChecked);
            }
        }
    };



}

