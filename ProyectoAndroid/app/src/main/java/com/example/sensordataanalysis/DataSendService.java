package com.example.sensordataanalysis;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.example.sensordataanalysis.Entidades.DataRecordsDao;
import com.example.sensordataanalysis.Entidades.MasterDao;
import com.example.sensordataanalysis.Entidades.DaoSession;
import com.example.sensordataanalysis.Entidades.SensorInformation;
import com.example.sensordataanalysis.Entidades.SensorInformationDTO;
import com.example.sensordataanalysis.Entidades.SensorInformationDao;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DataSendService extends BroadcastReceiver {
    static final String TAG = "DataSendService";
    private String usuario;

    @Override
    public void onReceive(Context context, Intent intent) {
        //Si reiniciamos el dispositivo, reiniciamos el servicio de recolección de datos
        Log.d(TAG,"Reloj activado");
        if ( Intent.ACTION_GET_CONTENT.equals(intent.getAction()) && intent.getAction().equals("android.intent.action.BOOT_COMPLETED"))
        {
            Log.d(TAG, "EnvioEstadisService: entra en el on bootreceiver");

            Intent collectDataIntent =  new Intent(context, DataStoreService.class);
            context.startService(collectDataIntent);
        }else
        {
            usuario = intent.getExtras().getString("Usuario", "null");
            Log.d(TAG,"El usuario es: "+usuario);
            sendSensorsData(context);
        }


    }

    public void sendSensorsData(Context context)
    {
        DaoSession daoSession = MasterDao.getInstance(context).getDaoSession();
        SensorInformationDao sensorInformationDao = daoSession.getSensorInformationDao();
        SensorInformation sensorInformation = sensorInformationDao.queryBuilder().where(SensorInformationDao.Properties.IdUsuario.eq(usuario)).unique();

        sensorInformation.toDTO();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Log.d(TAG, "JSON creado: " + gson.toJson(SensorInformationDTO.getInstance()));


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://immense-ravine-73674.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        DataRecordsInterface service = retrofit.create(DataRecordsInterface.class);
        Call<ResponseBody> call = service.sendInformation(SensorInformationDTO.getInstance());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful())
                {
                    Log.d(TAG,"Respuesta recibida desde el servidor. Limpiando datos");
                    Log.d(TAG, response.body().toString()); // have your all data
                    SensorInformationDTO.getInstance().limpiarLista();
                    Log.d(TAG,"Limpiada lista de SensorInformationDTO");
                    DaoSession daoSession = MasterDao.getInstance(context).getDaoSession();
                    DataRecordsDao dataRecordsDao = daoSession.getDataRecordsDao();
                    dataRecordsDao.deleteAll();
                    Log.d(TAG,"Limpiada base dadatos de data records");
                    SensorInformationDao sensorInformationDao = daoSession.getSensorInformationDao();
                    SensorInformation sensorInformation = sensorInformationDao.queryBuilder().where(SensorInformationDao.Properties.IdUsuario.eq(usuario)).unique();
                    sensorInformation.cleanDataRecords();
                    Log.d(TAG,"Limpiado datos de sensor information");

                }else{
                    Log.d(TAG,response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d(TAG, "Fail: "+t.getMessage());
            }
        });

    }

}
