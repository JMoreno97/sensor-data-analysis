package com.example.sensordataanalysis.Entidades;

import java.io.Serializable;

public class Sensor implements Serializable {

    private String nombre;
    private int tipo;
    private int tiempo;
    private boolean activo;

    public Sensor(){}
    public Sensor(String nombre, int tiempo, boolean activo, int tipo)
    {
        this.nombre = nombre;
        this.tiempo = tiempo;
        this.activo = activo;
        this.tipo = tipo;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public int getTiempo()
    {
        return this.tiempo;
    }

    public boolean getActivo()
    {
        return this.activo;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public void setTiempo(int tiempo)
    {
        this.tiempo = tiempo;
    }

    public void setActivo(boolean activo)
    {
        this.activo = activo;
    }

    public int getTipo() { return this.tipo; }

    public void setTipo(int tipo) { this.tipo = tipo; }

}
