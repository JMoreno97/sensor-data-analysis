package ujaen.seu.server.Services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import ujaen.seu.server.DTOs.DataRecordDTO;
import ujaen.seu.server.Repositories.DataRecordRepository;


@Service
public class SensorRetrieverService {


    Logger logger = LoggerFactory.getLogger(SensorRetrieverService.class);

    @Autowired
    DataRecordRepository dataRecordRepository;

    public Flux<DataRecordDTO> findDataRecords(String name, String sensor, Long minDate, Long maxDate) {

        logger.info("Name " + name);
        logger.info("sensor " + sensor);
        logger.info("minDate " + minDate);
        logger.info("maxDate " + maxDate);

        if (minDate == null || maxDate == null) {
            return dataRecordRepository.findDataRecordsByUserAndSensor(name, sensor)
                    .map(DataRecordDTO::new)
                    .log();
        } else {
            return dataRecordRepository.findDataRecordsByUserAndSensorAndMomentIsBetween(name, sensor, minDate, maxDate)
                    .map(DataRecordDTO::new)
                    .log();
        }
    }

    ;

    public Flux<DataRecordDTO> findALl() {

        return dataRecordRepository.findAll()
                .map(DataRecordDTO::new)
                .log();
    }


}
