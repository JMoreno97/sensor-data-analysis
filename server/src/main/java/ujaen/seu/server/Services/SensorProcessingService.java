package ujaen.seu.server.Services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ujaen.seu.server.DTOs.SensorInformationDTO;
import ujaen.seu.server.Entities.DataRecord;
import ujaen.seu.server.Repositories.DataRecordRepository;

@Service
public class SensorProcessingService {


    @Autowired
    DataRecordRepository dataRecordRepository;

    Logger logger = LoggerFactory.getLogger(SensorProcessingService.class);


    public Mono<Boolean> processSensorInformation(SensorInformationDTO sensorInformationDTO) {

        logger.info("User " + sensorInformationDTO.toString());


        return Flux.fromIterable(sensorInformationDTO.getDailyRecords())
                .map(dataRecordDTO -> DataRecord.dataRecordFromDTO(dataRecordDTO, sensorInformationDTO.getIdUsuario()))
                .flatMap(dataRecordRepository::save)
                .then(Mono.just(Boolean.TRUE))
                .onErrorReturn(Boolean.FALSE)
                .log();

    }


}
