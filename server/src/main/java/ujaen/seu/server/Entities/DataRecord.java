package ujaen.seu.server.Entities;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import ujaen.seu.server.DTOs.DataRecordDTO;

@Data
@Document
public class DataRecord {

    String user;

    String sensor;

    String medida;

    Long moment;

    String value;

    String type;

   static public DataRecord dataRecordFromDTO(DataRecordDTO dr, String user) {

        DataRecord drReturn = new DataRecord();
        drReturn.setUser(user);
        drReturn.setSensor(dr.getSensor());
        drReturn.setMoment(dr.getMoment());
        drReturn.setValue(dr.getValue());
        drReturn.setMedida(dr.getMedida());
        drReturn.setType(dr.getTipo());

        return drReturn;
    }

    ;

}
