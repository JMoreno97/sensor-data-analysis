package ujaen.seu.server.DTOs;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SensorInformationDTO {

    String idUsuario;
    List<DataRecordDTO> dailyRecords = new ArrayList<>();

    public boolean addValueToRecords(DataRecordDTO dr) {
        return dailyRecords.add(dr);
    }


    @Override
    public String toString() {
        String base =  "El id del usuario es: " + idUsuario + ", hay " + dailyRecords.size() + " valores en la lista";
        for(DataRecordDTO dr : dailyRecords){
            base += " dr Sensor:"+dr.getSensor()+ " Valor: "+ dr.getValue()
                    + " Moment "+ dr.getMoment() + " Unidad "+dr.getMedida();
        }

        return base;

    }

    //

}
