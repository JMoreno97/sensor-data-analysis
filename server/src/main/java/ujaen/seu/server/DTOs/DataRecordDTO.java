package ujaen.seu.server.DTOs;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ujaen.seu.server.Entities.DataRecord;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DataRecordDTO {

    String sensor;

    String medida;

    Long moment;

    String value;

    String tipo;


    public DataRecordDTO(DataRecord dataRecord) {
        this.sensor = dataRecord.getSensor();
        this.moment = dataRecord.getMoment();
        this.value = dataRecord.getValue();
        this.medida = dataRecord.getMedida();
        this.tipo = dataRecord.getType();
    }
}
