package ujaen.seu.server.Repositories;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import ujaen.seu.server.Entities.DataRecord;

public interface DataRecordRepository extends ReactiveCrudRepository<DataRecord, String> {




    Flux<DataRecord> findDataRecordsByUserAndSensor(String name,String sensor);

    Flux<DataRecord> findDataRecordsByUserAndSensorAndMomentIsBetween(String name, String sensor, Long minDate, Long maxDate);




}
