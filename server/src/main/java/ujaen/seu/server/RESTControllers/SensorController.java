package ujaen.seu.server.RESTControllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ujaen.seu.server.DTOs.DataRecordDTO;
import ujaen.seu.server.DTOs.SensorInformationDTO;
import ujaen.seu.server.Services.SensorProcessingService;
import ujaen.seu.server.Services.SensorRetrieverService;

@CrossOrigin
@RestController
@RequestMapping(value = "/sensor")
public class SensorController {

    @Autowired
    SensorProcessingService sensorProcessingService;

    @Autowired
    SensorRetrieverService sensorRetrieverService;

    Logger logger = LoggerFactory.getLogger(SensorController.class);


    @GetMapping(value = "/{test}")
    public String test(@PathVariable String test) {

        return "Hola " + test;
    }

    @PostMapping(value = "/data",
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity> getData(@RequestBody SensorInformationDTO sensorInformationDTO) {

        logger.info(sensorInformationDTO.toString());

        return sensorProcessingService.processSensorInformation(sensorInformationDTO)
                .map(r -> ResponseEntity.status(HttpStatus.OK).body("Information received"))
                .cast(ResponseEntity.class)
                .defaultIfEmpty(ResponseEntity.status(HttpStatus.CONFLICT).body(null));

    }

    @GetMapping(value = "/data",
            produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public Flux<DataRecordDTO> findDataRecords(@RequestParam(value = "user") String nombre,
                                               @RequestParam(value = "sensor") String sensor,
                                               @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Long minDate,
                                               @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Long maxDate) {


        return sensorRetrieverService.findDataRecords(nombre, sensor, minDate, maxDate);
    }


    @GetMapping(value = "/data/all",
            produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public Flux<DataRecordDTO> findDataRecords() {


        return sensorRetrieverService.findALl();
    }

}
