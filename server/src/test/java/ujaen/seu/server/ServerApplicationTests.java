package ujaen.seu.server;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ujaen.seu.server.DTOs.DataRecordDTO;
import ujaen.seu.server.DTOs.SensorInformationDTO;
import ujaen.seu.server.Services.SensorProcessingService;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@SpringBootTest
class ServerApplicationTests {

    @Test
    void contextLoads() {
    }

    Logger logger = LoggerFactory.getLogger(ServerApplicationTests.class);


    @Autowired
    SensorProcessingService sensorProcessingService;

    @Autowired
    WebTestClient webTestClient;


    @Test
    void insertSensorInformationDTO() {

        SensorInformationDTO s = new SensorInformationDTO();
        s.setIdUsuario("Juan");
        double maxT = 30;
        double minT = 10;


        for (int i = 0; i < 500; i++) {
            DataRecordDTO dr = new DataRecordDTO();
            ZonedDateTime zdt = ZonedDateTime.of(LocalDateTime.now().plusSeconds(i), ZoneId.systemDefault());
            dr.setMoment(zdt.toInstant().toEpochMilli());
            dr.setSensor("Temperatura");
            double value = (Math.random() * (maxT - minT)) + minT;
            dr.setValue(String.valueOf(value));
            s.addValueToRecords(dr);
        }

        double maxH = 90;
        double minH = 10;
        for (int i = 0; i < 500; i++) {
            DataRecordDTO dr = new DataRecordDTO();

            ZonedDateTime zdt = ZonedDateTime.of(LocalDateTime.now().plusSeconds(i), ZoneId.systemDefault());
            dr.setMoment(zdt.toInstant().toEpochMilli());
            dr.setSensor("Humedad");
            double value = (Math.random() * (maxH - minH)) + minH;
            dr.setValue(String.valueOf(value));
            s.addValueToRecords(dr);
        }

        String uri = "http://localhost:8080/sensor/data";
        webTestClient.post()
                .uri(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(s), SensorInformationDTO.class)
                .exchange()
                .expectStatus().isOk();

    }

    @Test
    void getData() {
        String name = "Juan";
        String sensor = "Temperatura";

        String uri = "http://localhost:8080/sensor/data?user=" + name + "&sensor=" + sensor;

        Flux<DataRecordDTO> response = webTestClient.get()
                .uri(uri)
                .exchange()
                .expectStatus().isOk()
                .returnResult(DataRecordDTO.class)
                .getResponseBody();


        Long elements = response.count().block();

        logger.info("Number of elements" + elements);


    }

    @Test
    void getDataWithTime() {
        String name = "Juan";
        String sensor = "Temperatura";
        LocalDateTime localDateTimeMin = LocalDateTime.of(2020, 3, 17, 10, 0);
        LocalDateTime localDateTimeMax = LocalDateTime.of(2020, 3, 17, 10, 15);
        Long min = ZonedDateTime.of(localDateTimeMin, ZoneId.systemDefault()).toInstant().toEpochMilli();
        Long max = ZonedDateTime.of(localDateTimeMax, ZoneId.systemDefault()).toInstant().toEpochMilli();


        String uri = "http://localhost:8080/sensor/data?user=" + name + "&sensor=" + sensor + "&minDate=" + min + "&maxDate=" + max;

        Flux<DataRecordDTO> response = webTestClient.get()
                .uri(uri)
                .exchange()
                .expectStatus().isOk()
                .returnResult(DataRecordDTO.class)
                .getResponseBody();


        Long elements = response.count().block();
        logger.info("Number of elements" + elements);

        assert (elements == 500L);


    }


}
