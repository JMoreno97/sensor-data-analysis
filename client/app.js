angular.module("App", ['main']);

angular.module("main", ['chart.js',"kendo.directives" ]);


/*
    Controlador principal
*/


angular.module("main").controller("mainController", function ($scope, $http, $q) {

    $scope.user = null;
    $scope.sensor = null;
    $scope.minDate = null;
    $scope.maxDate = null;

    $scope.message = null;

    $scope.responseReceived = null;
    $scope.resultsQuery = []

    $scope.getType = function (x) {
        return typeof x;
    };
    $scope.isDate = function (x) {
        return x instanceof Date;
    };




    $scope.getDataFromServer = function () {
        $scope.resultsQuery = [];
        $scope.responseReceived = null;
        $scope.labelsB = []
        $scope.labelsL = []
        $scope.labelsP = []
        $scope.dataB = []
        $scope.dataL = [[]]
        $scope.dataP = [[]]
        $scope.seriesB = []
        $scope.seriesL = []



        url = "http://immense-ravine-73674.herokuapp.com/sensor/data?";
        query = "user=" + $scope.user + "&sensor=" + $scope.sensor + "&minDate=" + $scope.minDate + "&maxDate=" + $scope.maxDate;


        minDate = new Date($scope.minDate).getTime();
        maxDate = new Date($scope.maxDate).getTime();

        console.log(minDate);
        console.log(maxDate);

        return $http({
            url: url,
            method: "GET",
            params: {
                user: $scope.user,
                sensor: $scope.sensor,
                minDate: minDate,
                maxDate: maxDate

            }

        }).then(function (response) {
            $scope.responseReceived = response.data;

        });

    };


    $scope.retrieve = function () {


        $scope.getDataFromServer();

        if ($scope.user != null && $scope.sensor != null) {

            var myPromise = $scope.getDataFromServer();

            myPromise.then(function (result) {
                var strLines = "";
                var parseando = false;
                try{
                    strLines = $scope.responseReceived.split("\n");
                    for(var i in strLines){
                        if(!parseando)
                        {
                            parseando = true;
                        }
                        console.log("Rellenando vector");
                        var obj = JSON.parse(strLines[i]);
                        $scope.resultsQuery.push(obj);
                    }
                }catch(error)
                {
                    if(!parseando)
                    {
                        console.error("Error parseando");
                        strLines = $scope.responseReceived;
                        $scope.resultsQuery.push(strLines);
                    }
                    
                }
                
            }
            );


        }
    };

    $scope.showData = function(){
        if ($scope.resultsQuery[0].sensor == "Battery"){
            //Dibujaremos 2 gráficos, barra y línea porque hay valores double y booleanos
            $scope.showBarData();
            $scope.showLineData();
        }else if ($scope.resultsQuery[0].sensor == "Light"){
            //Para la luminosidad utilizaremos diagrama de líneas. Solo hay valores double
            $scope.showLineData()
        }else if ($scope.resultsQuery[0].sensor == "Screen" || $scope.resultsQuery[0].sensor == "Wifi" || $scope.resultsQuery[0].sensor == "Bluetooth"){
            //Diagrama de barras, solo hay valores booleanos (0,1)
            $scope.showPieData();
            $scope.showBarData();
        }else if ( $scope.resultsQuery[0].sensor == "Timezone"){
            //Estos valores no sé como representarlos. Diagrama de quesos y diagrama de barras quizás
            $scope.showLocationTimeZonePieData();
        }else if($scope.resultsQuery[0].sensor = "Location")
        {
            getCities().then(function(result){
                for(index in $scope.resultsQuery)
                {
                    $scope.resultsQuery[index].value = result[index];
                }
                $scope.showLocationTimeZonePieData();
            })
            
        }
    }

    $scope.showBarData = function () {
        valoresOrdenados = Array.from($scope.resultsQuery).sort(compare);
        var labels = []
        var series = []
        for ( key in valoresOrdenados)
        {
            var date = $scope.dateFromMill(valoresOrdenados[key])
            if(!labels.includes(date))
            {
                labels.push(date)
            }
            
            if(valoresOrdenados[key].tipo == "Boolean")
            {
                if(!series.includes(valoresOrdenados[key].medida))
                {
                    series.push(valoresOrdenados[key].medida);
                }
            }
            
        }

        var data = [];
        for(medida in series){
            var vMedida = []
            for (label in labels)
            {
                vMedida.push(0);
            }

            for( key in valoresOrdenados)
            {
                if(valoresOrdenados[key].medida == series[medida])
                {
                    var index = labels.indexOf($scope.dateFromMill(valoresOrdenados[key]));
                    var value = 0;
                    if (valoresOrdenados[key].value == "true")
                    {
                        value = 1;
                    }
                    vMedida[index] = value;
                }
            }
            data.push(vMedida);
        }
        

        $scope.optionsB =  {
            title: {
                display: true,
                text: $scope.resultsQuery[0].sensor
            },
            legend:{
                display:true
            }
        }
        $scope.labelsB = labels
        $scope.seriesB = series
        $scope.dataB = data;
        console.log(data);
        $scope.colorsB = [ '#FF0000', '#1E90FF', '#FF4500', '#8A2BE2', '#FF69B4', '#FFFF00', '#4D5360'];
    }

    $scope.showLineData = function () {
    
        valoresOrdenados = Array.from($scope.resultsQuery).sort(compare);
        var labels = []
        var series = []
        for ( key in valoresOrdenados)
        {
            var date = $scope.dateFromMill(valoresOrdenados[key])
            if(!labels.includes(date))
            {
                labels.push(date)
            }

            if(valoresOrdenados[key].tipo == "Integer" || valoresOrdenados[key].tipo == "Double")
            {
                if(!series.includes(valoresOrdenados[key].medida))
                {
                    series.push(valoresOrdenados[key].medida);
                }
            }
            
            
        }

        var data = [];
        for(medida in series){
            var vMedida = []
            for (label in labels)
            {
                vMedida.push(0);
            }

            for( key in valoresOrdenados)
            {
                if(valoresOrdenados[key].medida == series[medida])
                {
                    var index = labels.indexOf($scope.dateFromMill(valoresOrdenados[key]));
                    vMedida[index] = valoresOrdenados[key].value;
                }
            }
            data.push(vMedida);
        }


        $scope.optionsL =  {
            title: {
                display: true,
                text: $scope.resultsQuery[0].sensor
            },
            legend:{
                display:true
            }
        }
        $scope.labelsL = labels;
        $scope.seriesL = series
        $scope.dataL = data
    }

    $scope.showPieData = function () {
        valoresOrdenados = Array.from($scope.resultsQuery).sort(compare);
        var labels = []
        for ( key in valoresOrdenados)
        {
             if(!labels.includes(valoresOrdenados[key].medida))
            {
                labels.push(valoresOrdenados[key].medida);
            }
             
        }
        var data = [];
        for(label in labels)
        {
            data[label] = 0;
        }

        for(label in labels){
            for( key in valoresOrdenados)
            {
                if(valoresOrdenados[key].medida == labels[label])
                {
                    var index = labels.indexOf(valoresOrdenados[key].medida);
                    data[index] += 1 
                }
            }
        }

        $scope.optionsP =  {
            title: {
                display: true,
                text: $scope.resultsQuery[0].sensor
            },
            legend:{
                display:true
            }
        }
        $scope.labelsP = labels;
        $scope.dataP = data;
        $scope.colorsB = [ '#FF0000', '#1E90FF', '#FF4500', '#8A2BE2', '#FF69B4', '#FFFF00', '#4D5360'];
    }

    $scope.showLocationTimeZonePieData = function () {
        valoresOrdenados = Array.from($scope.resultsQuery).sort(compare);
        var labels = []
        for ( key in valoresOrdenados)
        {
            if(!labels.includes(valoresOrdenados[key].value))
            {
                labels.push(valoresOrdenados[key].value)
            }
        }

        var data = [];
        for(label in labels)
        {
            data[label] = 0;
        }

        for(label in labels){
            for( key in valoresOrdenados)
            {
                if(valoresOrdenados[key].value == labels[label])
                {
                    var index = labels.indexOf(valoresOrdenados[key].value);
                    data[index] += 1 
                }
            }
        }

        $scope.optionsP =  {
            title: {
                display: true,
                text: $scope.resultsQuery[0].sensor
            },
            legend:{
                display:true
            }
        }
        $scope.labelsP = labels;
        $scope.dataP = data;
        $scope.colorsB = [ '#FF0000', '#1E90FF', '#FF4500', '#8A2BE2', '#FF69B4', '#FFFF00', '#4D5360'];
    }

    $scope.dateFromMill = function(obj){
        p = new Date(obj.moment)
        return p.toLocaleString();
    }

    function compare( a, b ) {
        if ( a.moment < b.moment ){
          return -1;
        }
        if ( a.moment > b.moment ){
          return 1;
        }
        return 0;
    }

    getCities = function() {

        var deferred = $q.defer();
        var ciudades = []
        var cont = 0;
        for(index in $scope.resultsQuery)
        {
            var coordsVector = $scope.resultsQuery[index].value.split(",");
            var lat = parseFloat(coordsVector[0]);
            var long = parseFloat(coordsVector[1]);
            var mapboxClient = mapboxSdk({ accessToken: "pk.eyJ1Ijoiam1hMDAwNDkiLCJhIjoiY2s4N2t4bzd3MGRsYjNnczlmZmU5aWh2biJ9.-PlT7qo-UJ9-xPkdYwoRmg" });
            mapboxClient.geocoding.reverseGeocode({
                query: [long, lat]
              })
                .send()
                .then(response => {
                    cont++;
                  // GeoJSON document with geocoding matches
                    const match = response.body;
                    ciudades.push(match.features[2].place_name);
                    if(cont == $scope.resultsQuery.length)
                    {
                        deferred.resolve(ciudades);
                    }
                    
                });
          }
          return deferred.promise;
    
        }
       
});
